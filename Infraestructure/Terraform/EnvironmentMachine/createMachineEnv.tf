variable "prefix" {
  default = "enviromentjvm"
}
variable "admin_ssh_publickey" {
  type        = "string"
  description = "Configure all the linux virtual machines in the cluster with the SSH RSA public key string. The key should include three parts, for example 'ssh-rsa AAAAB...snip...UcyupgH azureuser@linuxvm"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKNclYZUjXiyfD1bXtz125A7MBnhkWJi6NtMr5/1/hIUhCX4kuy5XACC99FPstIjSmk+fp2zCsm+b3GfTyIgto4e2AfHFWc2DNEh4jncPWcmnhMp2h5OhOvsHJlVMVYRpwJoMYrzHp52LiUk0vqVMtsJpwnDpRipOEoO0TYQ73JmrZN3rjDNEohI6X7029ycl8/DIebwUJSIL1uF+r+KMQ6kogzMQLuOokM4hDJFJXmRQsiycAKTemVFWdJWafvR2+fO1xQ5X0veJ0WLonP9RVsK222SREEAlYFF6pvGL8ZmlWD8vN2hUwctrxBSXZk7eNl3nFu0UBam/MHQVaOF4f kalugy@kalugypc"
}

resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-resources"
  location = "East US"
}

resource "azurerm_public_ip" "publicip" {
  name                = "publicip"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  allocation_method   = "Static"
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = "${azurerm_resource_group.main.name}"
  virtual_network_name = "${azurerm_virtual_network.main.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${azurerm_subnet.internal.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.publicip.id}"
  }
}

resource "azurerm_virtual_machine" "main" {
  name                  = "${var.prefix}-vm"
  location              = "${azurerm_resource_group.main.location}"
  resource_group_name   = "${azurerm_resource_group.main.name}"
  network_interface_ids = ["${azurerm_network_interface.main.id}"]
  vm_size               = "Standard_D2s_v3"

  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "hostname"
    admin_username = "adminuser"
  }
  os_profile_linux_config {
    disable_password_authentication = true 
    
    ssh_keys {
      path     = "/home/adminuser/.ssh/authorized_keys"
      key_data = "${var.admin_ssh_publickey}"
    }
  }
  tags = {
    environment = "staging"
  }
}
output "public_ip" {
  value = "${azurerm_public_ip.publicip.ip_address}"
}